//
//  ViewController.swift
//  ShowImages
//
//  Created by Sibani Rani Rath on 14/04/24.
//

import UIKit

struct MediaCoverage: Codable {
    let imageUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case imageUrl = "coverageURL"
    }
}

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var imageCache = NSCache<NSString, UIImage>()
    var arrImages: [MediaCoverage] = []
    
    let apiUrl = "https://acharyaprashant.org/api/v2/content/misc/media-coverages?limit=100"

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initDesign()
    }
    
    func initDesign() {
        //Manage for iPhone X
        if UIScreen.main.bounds.height >= 812 {
            navBarHeightConstraint.constant = 92
        }
        
        // Configure collection view
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // Call function to fetch data
        fetchImages()
    }
    
    func fetchImages(){
        guard let url = URL(string: apiUrl) else { return }
        
        URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
            guard let self = self else { return }
            
            if let error = error {
                print("Error fetching images:", error.localizedDescription)
                return
            }
            
            guard let data = data else { return }
            do {
                let decoder = JSONDecoder()
                self.arrImages = try decoder.decode([MediaCoverage].self, from: data)
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            } catch {
                print("Error decoding JSON:", error.localizedDescription)
            }
        }.resume()
    }
    
    //MARK: CollectionView Delegate & DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as? ImageCell else {
            return UICollectionViewCell()
        }
        
        cell.imageView.layer.borderColor = UIColor.lightGray.cgColor
        cell.imageView.layer.borderWidth = 0.3
        cell.imageView.clipsToBounds = true
        
        let mediaCoverage = arrImages[indexPath.item]
        if let imageUrl = mediaCoverage.imageUrl, let cachedImage = imageCache.object(forKey: imageUrl as NSString) {
            cell.imageView.image = cachedImage
        } else if let imageUrl = mediaCoverage.imageUrl, let url = URL(string: imageUrl) {
            DispatchQueue.global().async {
                if let imageData = try? Data(contentsOf: url), let image = UIImage(data: imageData) {
                    self.imageCache.setObject(image, forKey: imageUrl as NSString)
                    DispatchQueue.main.async {
                        if let cellToUpdate = collectionView.cellForItem(at: indexPath) as? ImageCell {
                            cellToUpdate.imageView.image = image
                        }
                    }
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 3 - 5 // 3 columns with 5 spacing between cells
        return CGSize(width: width, height: width)
    }
    
}

