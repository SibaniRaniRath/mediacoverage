//
//  ImageCell.swift
//  ShowImages
//
//  Created by Sibani Rani Rath on 14/04/24.
//

import UIKit

class ImageCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
}
